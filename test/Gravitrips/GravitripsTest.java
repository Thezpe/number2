package Gravitrips;

import org.junit.Assert;
import org.junit.Test;

public class GravitripsTest {

	@Test
	public void wonGeneralDiagTest() {
		
		Field f = new Field();
		Move m = new Move();
		Player hp = new HumanPlayer('X');

		for (int row = 4, column = 0; row >= 0 && column < f.getColumns(); row--, column++) {
			m.setRow(row);
			m.setColumn(column);
			f.applyMove(m, hp);
		}
		Assert.assertEquals(f.wonGeneralDiag(hp), f.won(hp));
	}
	
	public void wonGeneralDiagTest2() {

	}
	
}
