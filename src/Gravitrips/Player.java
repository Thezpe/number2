package Gravitrips;

public abstract class Player {

	private char symbol;

	Player(char symbol) {
		this.symbol = symbol;
	}

	public char getSymbol() {
		return symbol;
	}

	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}

	public abstract Move move(int column, int row);
}

class HumanPlayer extends Player {

	HumanPlayer(char symbol) {
		super(symbol);
	}

	@Override
	public Move move(int column, int row) {
		return new Move(column, row);
	}
}

class ComputerPlayer extends Player {

	ComputerPlayer(char symbol) {
		super(symbol);
	}

	@Override
	public Move move(int column, int row) {
		return new Move(column, row);
	}
}