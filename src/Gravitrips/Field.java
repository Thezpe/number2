package Gravitrips;

public class Field {

	private static final char BLANK = '.';
	private static final int FOUR = 4;

	private static final int ROWS = 5;
	private static final int COLUMNS = 7;
	protected char[][] field = new char[ROWS][COLUMNS];

	public char[][] getField() {
		return field;
	}

	public int getRows() {
		return ROWS;
	}

	public int getColumns() {
		return COLUMNS;
	}

	public char getBlank() {
		return BLANK;
	}

	public void printField() {

		for (int column = 0; column < COLUMNS; column++) {
			System.out.print((column + 1) + " ");
		}
		System.out.println();
		for (int row = 0; row < ROWS; row++) {
			for (int column = 0; column < COLUMNS; column++) {
				System.out.print(field[row][column] + " ");
			}
			System.out.println();
		}
	}

	public void fillFieldWithBlanks() {
		for (int row = 0; row < ROWS; row++) {
			for (int column = 0; column < COLUMNS; column++) {
				field[row][column] = BLANK;
			}
		}
	}

	public boolean checkForBlank(int row, int column) {
		if (field[row][column] != BLANK) {
			return false;
		}
		return true;
	}

	public void applyMove(Move m, Player player) {
		this.field[m.getRow()][m.getColumn()] = player.getSymbol();
	}

	/* Win part */

	public boolean draw() {

		for (int row = 0; row < getRows(); row++) {
			for (int column = 0; column < getColumns(); column++) {
				if (getField()[row][column] == getBlank()) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean won(Player player) {

		return wonColumn(player) || wonRow(player) || wonGeneralDiag(player)
				|| wonSecondDiag(player);
	}

	public boolean wonColumn(Player player) {

		for (int column = 0; column < getColumns(); column++) {
			if (checkColumn(player, column)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkColumn(Player player, int column) {

		int count = 0;
		for (int row = 0; row < getRows(); row++) {
			if (getField()[row][column] == player.getSymbol()) {
				count++;
			} else {
				count = 0;
			}
			if (count == FOUR) {
				return true;
			}
		}
		return false;
	}

	public boolean wonRow(Player player) {

		for (int row = 0; row < getRows(); row++) {
			if (checkRow(row, player)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkRow(int row, Player player) {

		int count = 0;
		for (int column = 0; column < getColumns(); column++) {
			if (getField()[row][column] == player.getSymbol()) {
				count++;
			} else {
				count = 0;
			}
			if (count == FOUR) {
				return true;
			}
		}
		return false;
	}

	public boolean wonGeneralDiag(Player player) {

		for (int row = getRows() - 1; row > 0; row--) {
			if (checkGeneralDiag(row, player)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkGeneralDiag(int row, Player player) {

		int count = 0;
		for (int column = 0; column < getColumns(); column++) {

			if (getField()[row][column] == player.getSymbol()) {
				count++;
				row--;
				if (row < 0) {
					row = getRows() - 1;
				}
			} else {
				count = 0;
			}
			if (count == FOUR) {
				return true;
			}
		}
		return false;
	}

	public boolean wonSecondDiag(Player player) {

		for (int row = getRows() - 1; row > 0; row--) {
			if (checkSecondDiag(row, player)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkSecondDiag(int row, Player player) {

		int count = 0;
		for (int column = getColumns() - 1; column > 0; column--) {
			if (getField()[row][column] == player.getSymbol()) {
				count++;
				row--;
				if (row < 0) {
					row = getRows() - 1;
				}
			} else {
				count = 0;
			}
			if (count == FOUR) {
				return true;
			}
		}
		return false;
	}

}