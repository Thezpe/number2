package Gravitrips;

import java.util.Scanner;

public class GameConsole {

	private Scanner scanner = new Scanner(System.in);

	private Field f = new Field();
	private Player humanPlayer = new HumanPlayer('X');
	private Player computerPlayer = new ComputerPlayer('O');

	/* Game */

	public void gameGravitrips() {

		f.fillFieldWithBlanks();

		int row = f.getRows() - 1;
		int column;

		do {

			f.printField();

			/* human move */
			if (!f.won(computerPlayer)) {
				column = humanInput();
				if (!f.checkForBlank(row, column)) {
					nextInputIfFieldIsBusy(column, row, humanPlayer);
				} else {
					f.applyMove(humanPlayer.move(column, row), humanPlayer);
				}
			}

			/* computer move */
			if (!f.won(humanPlayer)) {
				column = computerInput();
				if (!f.checkForBlank(row, column)) {
					nextInputIfFieldIsBusy(column, row, computerPlayer);
				} else {
					f.applyMove(computerPlayer.move(column, row),
							computerPlayer);
				}
			}

		} while (!f.won(humanPlayer) && !f.won(computerPlayer) && !f.draw());

		f.printField();

		if (f.won(humanPlayer)) {
			System.out.println("PLAYER WON!");
		}
		if (f.won(computerPlayer)) {
			System.out.println("COMPUTER WON!");
		} else if (f.draw()) {
			System.out.println("DRAW!");
		}
	}

	private int humanInput() {

		System.out.print("Enter column from 1 to 7: ");
		int column = scanner.nextInt();
		column = column - 1;
		return column;
	}

	private int computerInput() {

		int column = (int) (Math.random() * 7);
		return column;
	}

	/* Field busy or OutOfBoundsException */

	private void nextInputIfFieldIsBusy(int column, int row, Player player) {

		boolean canMove = false;

		do {
			if (player == humanPlayer && !f.checkForBlank(row, column)) {
				row--;

				if (row < 0) {
					System.out.println("This field is busy!");
					column = humanInput();
					row = f.getRows() - 1;
				}
			} else {
				if (player == computerPlayer && !f.checkForBlank(row, column)) {
					row--;
					if (row < 0) {
						column = computerInput();
						row = f.getRows() - 1;
					}
				}
			}
			if (f.checkForBlank(row, column)) {
				canMove = true;
			}
		} while (!canMove && !f.draw());
		f.applyMove(player.move(column, row), player);
	}
}