package Gravitrips;

public class Move {

	int column;
	int row;

	public Move(int column, int row) {
		super();
		this.column = column;
		this.row = row;
	}

	public Move() {

	}

	public int getColumn() {
		return column;
	}

	public int getRow() {
		return row;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public void setRow(int row) {
		this.row = row;
	}
	
}